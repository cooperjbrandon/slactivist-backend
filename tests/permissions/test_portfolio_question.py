import json

from model_mommy import mommy

from common.models import (
    Portfolio,
    PortfolioQuestion,
    PortfolioQuestionComposition,
    User,
)
from tests.mixins import BaseApiSetUp


class PortfolioQuestionPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(PortfolioQuestionPermissionTestCase, self).setUp()
        self.pf1 = mommy.make(PortfolioQuestion)
        self.pf2 = mommy.make(PortfolioQuestion)
        self.pf3 = mommy.make(PortfolioQuestion)
        self.pf4 = mommy.make(PortfolioQuestion)

        self.portfolio1 = mommy.make(Portfolio, profile=self.user.profile)
        self.pqc1 = self._make_pqc(self.portfolio1, self.pf1, 1)
        self.pqc2 = self._make_pqc(self.portfolio1, self.pf2, 2)
        self.pqc3 = self._make_pqc(self.portfolio1, self.pf3, 3)

        self.user2 = mommy.make(User)
        self.portfolio2 = mommy.make(Portfolio, profile=self.user2.profile)
        self.pqc4 = self._make_pqc(self.portfolio2, self.pf1, 4)
        self.pqc5 = self._make_pqc(self.portfolio2, self.pf2, 5)
        self.pqc6 = self._make_pqc(self.portfolio2, self.pf3, 6)

    def _make_pqc(self, portfolio, question, score):
        return mommy.make(
            PortfolioQuestionComposition,
            portfolio=portfolio,
            portfolio_question=question,
            score=score
        )

    def test_portfolio_focus_is_readonly(self):
        data = {
            'name': 'hihi'
        }

        # create should fail
        response = self.client.post('/api/portfolio_questions', data)
        self.assertEqual(response.status_code, 405)

        # put should fail
        response = self.client.put('/api/portfolio_questions/%s' % self.pf1.id, data)
        self.assertEqual(response.status_code, 405)

        # patch should fail
        response = self.client.patch('/api/portfolio_questions/%s' % self.pf1.id, data)
        self.assertEqual(response.status_code, 405)

        # delete should fail
        response = self.client.delete('/api/portfolio_questions/%s' % self.pf1.id)
        self.assertEqual(response.status_code, 405)

        # read should succeed
        response = self.client.get('/api/portfolio_questions')
        self.assertEqual(response.status_code, 200)

    def test_sideload_composition_and_portfolios_returns_correct_data(self):

        url = '/api/portfolio_questions?include[]=portfolio_question_compositions.portfolio.'
        response = self.client.get(url)
        content = json.loads(response.content)

        question_ids = [question.get('id') for question in content['portfolio_questions']]
        composition_ids = [comp.get('id') for comp in content['portfolio_question_compositions']]
        portfolio_ids = [portfolio.get('id') for portfolio in content['portfolios']]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(question_ids), 4)
        self.assertEqual(len(composition_ids), 3)
        self.assertEqual(len(portfolio_ids), 1)

        self.assertTrue(self.pqc1.id in composition_ids)
        self.assertTrue(self.pqc2.id in composition_ids)
        self.assertTrue(self.pqc3.id in composition_ids)
        self.assertTrue(self.portfolio1.id in portfolio_ids)
