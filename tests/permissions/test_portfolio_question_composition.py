import json

from model_mommy import mommy

from common.models import (
    Portfolio,
    PortfolioQuestion,
    PortfolioQuestionComposition
)
from tests.mixins import BaseApiSetUp


class PortfolioQuestionCompositionPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(PortfolioQuestionCompositionPermissionTestCase, self).setUp()

        self.pq1 = mommy.make(PortfolioQuestion)
        self.pq2 = mommy.make(PortfolioQuestion)
        self.pq3 = mommy.make(PortfolioQuestion)

        # create compositions for current user
        self.portfolio = mommy.make(Portfolio, profile=self.user.profile)
        self.pqc1 = mommy.make(
            PortfolioQuestionComposition,
            portfolio=self.portfolio,
            portfolio_question=self.pq1,
            score=3
        )
        self.pqc2 = mommy.make(
            PortfolioQuestionComposition,
            portfolio=self.portfolio,
            portfolio_question=self.pq2,
            score=6
        )

        # create compositions for other user
        self.portfolio2 = mommy.make(Portfolio, profile=self.superuser.profile)
        self.pqc3 = mommy.make(
            PortfolioQuestionComposition,
            portfolio=self.portfolio2,
            portfolio_question=self.pq1,
            score=3
        )
        self.pqc4 = mommy.make(
            PortfolioQuestionComposition,
            portfolio=self.portfolio2,
            portfolio_question=self.pq2,
            score=6
        )

    def test_can_view_only_own_portfolio_question_comps(self):
        response = self.client.get('/api/portfolio_question_compositions')
        content = json.loads(response.content)
        composition_ids = [comp.get('id') for comp in content['portfolio_question_compositions']]
        self.assertEqual(len(composition_ids), 2)
        self.assertTrue(self.pqc1.id in composition_ids)
        self.assertTrue(self.pqc2.id in composition_ids)

    def test_can_create_portfolio_question_comp_only_with_portfolios_belonging_to_own_profile(self):
        data = {
            'portfolio': self.user.profile.id,
            'portfolio_question': self.pq3.id,
            'score': 2
        }
        response = self.client.post('/api/portfolio_question_compositions', data)
        self.assertEqual(response.status_code, 201)

        data['portfolio'] = self.portfolio2.id
        response = self.client.post('/api/portfolio_question_compositions', data)
        self.assertEqual(response.status_code, 400)

    def test_can_only_update_own_portfolio_question_composition(self):
        data = {
            'score': 4
        }
        response = self.client.patch('/api/portfolio_question_compositions/%s' % self.pqc1.id, data)
        self.assertEqual(response.status_code, 200)

        # now try updating someone else's portfolio question composition
        response = self.client.patch('/api/portfolio_question_compositions/%s' % self.pqc3.id, data)
        self.assertEqual(response.status_code, 404)

    def test_cannot_update_portfolio_property(self):
        data = {
            'portfolio': self.portfolio2.id
        }
        response = self.client.patch('/api/portfolio_question_compositions/%s' % self.pqc1.id, data)
        self.assertEqual(response.status_code, 400)

    def test_cannot_update_portfolio_question_property(self):
        data = {
            'portfolio_question': self.pq3.id
        }
        response = self.client.patch('/api/portfolio_question_compositions/%s' % self.pqc1.id, data)
        self.assertEqual(response.status_code, 400)

    def test_cannot_update_portfolio_question_composition_when_portfolio_marked_as_ready(self):
        data = {
            'score': 5
        }
        self.portfolio.ready = True
        self.portfolio.save()
        response = self.client.patch('/api/portfolio_question_compositions/%s' % self.pqc1.id, data)
        self.assertEqual(response.status_code, 400)

    def test_cannot_delete_portfolio_question_composition(self):
        # delete should fail
        response = self.client.delete('/api/portfolio_question_compositions/%s' % self.portfolio.id)
        self.assertEqual(response.status_code, 405)
