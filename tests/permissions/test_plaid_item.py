import json

from model_mommy import mommy

from common.models import (
    PlaidItem,
    User,
)
from tests.mixins import BaseApiSetUp


class PlaidItemPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(PlaidItemPermissionTestCase, self).setUp()

        self.profile = self.user.profile
        self.different_profile = mommy.make(User).profile

        self.plaid_item1 = mommy.make(PlaidItem, profile=self.profile)
        self.plaid_item2 = mommy.make(PlaidItem, profile=self.profile)
        self.different_plaid_item = mommy.make(PlaidItem, profile=self.different_profile)

    def test_can_view_only_own_plaid_items(self):
        response = self.client.get('/api/plaid_items')
        content = json.loads(response.content)
        plaid_item_ids = [plaid_item.get('id') for plaid_item in content['plaid_items']]
        self.assertEqual(len(plaid_item_ids), 2)
        self.assertTrue(self.plaid_item1.id in plaid_item_ids)
        self.assertTrue(self.plaid_item2.id in plaid_item_ids)

    def test_can_create_plaid_item_only_with_own_profile(self):
        data = {
            'profile': self.profile.id,
            'access_token': 'abc',
            'institution_id': 'abcd',
            'institution_name': 'abcdef'
        }
        response = self.client.post('/api/plaid_items', data)
        self.assertEqual(response.status_code, 201)

        data['profile'] = self.different_profile.id
        response = self.client.post('/api/plaid_items', data)
        self.assertEqual(response.status_code, 400)

    def test_cannot_update_plaid_item(self):
        # update should fail
        data = {
            'access_token': 'zyxw'
        }
        response = self.client.patch('/api/plaid_items/%s' % self.plaid_item1.id, data)
        self.assertEqual(response.status_code, 405)

    def test_cannot_delete_plaid_item(self):
        # delete should fail
        response = self.client.delete('/api/plaid_items/%s' % self.plaid_item1.id)
        self.assertEqual(response.status_code, 405)
