import json

from model_mommy import mommy

from common.models import (
    Organization,
    OrganizationPortfolioComposition,
    Portfolio,
)
from tests.mixins import BaseApiSetUp


class OrganizationPortfolioCompositionPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(OrganizationPortfolioCompositionPermissionTestCase, self).setUp()

        self.o1 = mommy.make(Organization)
        self.o2 = mommy.make(Organization)
        self.o3 = mommy.make(Organization)

        # create compositions for current user
        self.portfolio = mommy.make(Portfolio, profile=self.user.profile)
        self.oc1 = mommy.make(
            OrganizationPortfolioComposition,
            portfolio=self.portfolio,
            organization=self.o1,
            order=3
        )
        self.oc2 = mommy.make(
            OrganizationPortfolioComposition,
            portfolio=self.portfolio,
            organization=self.o2,
            order=6
        )

        # create compositions for other user
        self.portfolio2 = mommy.make(Portfolio, profile=self.superuser.profile)
        self.oc3 = mommy.make(
            OrganizationPortfolioComposition,
            portfolio=self.portfolio2,
            organization=self.o1,
            order=3
        )
        self.oc4 = mommy.make(
            OrganizationPortfolioComposition,
            portfolio=self.portfolio2,
            organization=self.o2,
            order=6
        )

    def test_can_view_only_own_organizatio_portfolio_comps(self):
        response = self.client.get('/api/organization_portfolio_compositions')
        content = json.loads(response.content)
        composition_ids = [comp.get('id') for comp in content['organization_portfolio_compositions']]
        self.assertEqual(len(composition_ids), 2)
        self.assertTrue(self.oc1.id in composition_ids)
        self.assertTrue(self.oc2.id in composition_ids)

    def test_organization_portfolio_composition_is_read_or_update_only(self):
        data = {
            'is_manual_add': True
        }

        # create should fail
        response = self.client.post('/api/organization_portfolio_compositions', data)
        self.assertEqual(response.status_code, 405)

        # delete should fail
        response = self.client.delete('/api/organization_portfolio_compositions/%s' % self.oc1.id)
        self.assertEqual(response.status_code, 405)

        # put should succeed
        response = self.client.put('/api/organization_portfolio_compositions/%s' % self.oc1.id, data)
        self.assertEqual(response.status_code, 200)

        # patch should succeed
        response = self.client.patch('/api/organization_portfolio_compositions/%s' % self.oc1.id, data)
        self.assertEqual(response.status_code, 200)

        # read should succeed
        response = self.client.get('/api/organization_portfolio_compositions')
        self.assertEqual(response.status_code, 200)

    def test_sideload_composition_returns_correct_data(self):

        url = '/api/organizations?include[]=organization_portfolio_compositions.'
        response = self.client.get(url)
        content = json.loads(response.content)

        composition_ids = [comp.get('id') for comp in content['organization_portfolio_compositions']]
        organization_ids = [organization.get('id') for organization in content['organizations']]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(composition_ids), 2)
        self.assertEqual(len(organization_ids), 3)
        self.assertEqual(len(organization_ids), Organization.objects.count())

        self.assertTrue(self.oc1.id in composition_ids)
        self.assertTrue(self.oc2.id in composition_ids)

    def test_can_only_update_own_org_portfolio_comp(self):
        data = {
            'active': True
        }
        response = self.client.patch('/api/organization_portfolio_compositions/%s' % self.oc1.id, data)
        self.assertEqual(response.status_code, 200)

        # now try updating someone else's org_portfolio_composition
        data = {
            'active': True
        }
        response = self.client.patch('/api/organization_portfolio_compositions/%s' % self.oc3.id, data)
        self.assertEqual(response.status_code, 404)

    def test_cannot_remove_last_org_from_portfolio(self):
        self.oc1.active = True
        self.oc2.active = True
        self.oc1.save()
        self.oc2.save()

        data = {
            'active': False,
            'is_removed': True
        }

        response = self.client.patch('/api/organization_portfolio_compositions/%s/' % self.oc1.id, data)
        self.assertEqual(response.status_code, 200)

        # last org_port_comp that is true. so should fail.
        response = self.client.patch('/api/organization_portfolio_compositions/%s/' % self.oc2.id, data)
        self.assertEqual(response.status_code, 400)
