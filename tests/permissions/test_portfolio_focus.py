from model_mommy import mommy

from common.models import (
    PortfolioFocus
)
from tests.mixins import BaseApiSetUp


class PortfolioFocusPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(PortfolioFocusPermissionTestCase, self).setUp()
        self.pf = mommy.make(PortfolioFocus)
        mommy.make(PortfolioFocus)

    def test_portfolio_focus_is_readonly(self):
        data = {
            'name': 'hihi'
        }

        # create should fail
        response = self.client.post('/api/portfolio_focuses', data)
        self.assertEqual(response.status_code, 405)

        # put should fail
        response = self.client.put('/api/portfolio_focuses/%s' % self.pf.id, data)
        self.assertEqual(response.status_code, 405)

        # patch should fail
        response = self.client.patch('/api/portfolio_focuses/%s' % self.pf.id, data)
        self.assertEqual(response.status_code, 405)

        # delete should fail
        response = self.client.delete('/api/portfolio_focuses/%s' % self.pf.id)
        self.assertEqual(response.status_code, 405)

        # read should succeed
        response = self.client.get('/api/portfolio_focuses')
        self.assertEqual(response.status_code, 200)
