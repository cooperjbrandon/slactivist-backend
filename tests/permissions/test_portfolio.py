import json

from model_mommy import mommy

from common.models import (
    Portfolio,
    PortfolioFocus,
    User,
)
from tests.mixins import BaseApiSetUp


class PortfolioPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(PortfolioPermissionTestCase, self).setUp()

        self.profile = self.user.profile
        self.different_profile = mommy.make(User).profile

        self.focus1 = mommy.make(PortfolioFocus)
        self.focus2 = mommy.make(PortfolioFocus)
        self.focus3 = mommy.make(PortfolioFocus)

        self.portfolio1 = mommy.make(Portfolio, profile=self.profile, focus=self.focus1)
        self.portfolio2 = mommy.make(Portfolio, profile=self.profile, focus=self.focus2)
        self.different_portfolio = mommy.make(Portfolio, profile=self.different_profile, focus=self.focus1)

    def test_can_view_only_own_portfolios(self):
        response = self.client.get('/api/portfolios')
        content = json.loads(response.content)
        portfolio_ids = [portfolio.get('id') for portfolio in content['portfolios']]
        self.assertEqual(len(portfolio_ids), 2)
        self.assertTrue(self.portfolio1.id in portfolio_ids)
        self.assertTrue(self.portfolio2.id in portfolio_ids)

    def test_can_create_portfolio_only_with_own_profile(self):
        data = {
            'focus': self.focus3.id,
            'profile': self.profile.id,
            'name': 'hi'
        }
        response = self.client.post('/api/portfolios', data)
        self.assertEqual(response.status_code, 201)

        data['profile'] = self.different_profile.id
        response = self.client.post('/api/portfolios', data)
        self.assertEqual(response.status_code, 400)

    def test_can_only_update_own_portfolio(self):
        data = {
            'focus': self.focus3.id
        }
        response = self.client.patch('/api/portfolios/%s' % self.portfolio1.id, data)
        self.assertEqual(response.status_code, 200)

        # now try updating someone else's portfolio
        data = {
            'focus': self.focus3.id
        }
        response = self.client.patch('/api/portfolios/%s' % self.different_portfolio.id, data)
        self.assertEqual(response.status_code, 404)

    def test_can_only_update_portfolio_focus_if_portfolio_is_not_ready(self):
        self.portfolio1.ready = True
        self.portfolio1.save()
        data = {
            'focus': self.focus3.id
        }
        response = self.client.patch('/api/portfolios/%s' % self.portfolio1.id, data)
        self.assertEqual(response.status_code, 400)

    def test_cannot_update_profile_property(self):
        data = {
            'profile': self.different_profile.id
        }
        response = self.client.patch('/api/portfolios/%s' % self.portfolio1.id, data)
        self.assertEqual(response.status_code, 400)

    def test_cannot_delete_portfolio(self):
        # delete should fail
        response = self.client.delete('/api/portfolios/%s' % self.portfolio1.id)
        self.assertEqual(response.status_code, 405)
