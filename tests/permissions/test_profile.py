import json

from model_mommy import mommy

from common.models import (
    User
)
from tests.mixins import BaseApiSetUp


class ProfilePermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(ProfilePermissionTestCase, self).setUp()
        self.profile = self.user.profile
        mommy.make(User)  # this will create another profile
        mommy.make(User)  # this will create another profile

    def test_can_view_only_own_profile(self):
        response = self.client.get('/api/profiles')
        content = json.loads(response.content)
        profile_ids = [profile.get('id') for profile in content['profiles']]
        self.assertEqual(len(profile_ids), 1)
        self.assertTrue(self.profile.id in profile_ids)

    def test_profile_is_readonly(self):
        data = {
            'user': self.superuser.id,
        }

        # create should fail
        response = self.client.post('/api/profiles', data)
        self.assertEqual(response.status_code, 405)

        # put should fail
        response = self.client.put('/api/profiles/%s' % self.profile.id, data)
        self.assertEqual(response.status_code, 405)

        # patch should fail
        response = self.client.patch('/api/profiles/%s' % self.profile.id, data)
        self.assertEqual(response.status_code, 405)

        # delete should fail
        response = self.client.delete('/api/profiles/%s' % self.profile.id)
        self.assertEqual(response.status_code, 405)

        # read should succeed
        response = self.client.get('/api/profiles')
        self.assertEqual(response.status_code, 200)
