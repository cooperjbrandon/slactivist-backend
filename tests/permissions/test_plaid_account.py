import json

from model_mommy import mommy

from common.models import (
    PlaidAccount,
    PlaidItem,
    User,
)
from tests.mixins import BaseApiSetUp


class PlaidAccountPermissionTestCase(BaseApiSetUp):

    def setUp(self):
        super(PlaidAccountPermissionTestCase, self).setUp()

        self.profile = self.user.profile
        self.different_profile = mommy.make(User).profile

        self.plaid_item1 = mommy.make(PlaidItem, profile=self.profile)
        self.different_plaid_item = mommy.make(PlaidItem, profile=self.different_profile)

        self.plaid_account1 = mommy.make(PlaidAccount, item=self.plaid_item1)
        self.plaid_account2 = mommy.make(PlaidAccount, item=self.plaid_item1)
        self.plaid_account3 = mommy.make(PlaidAccount, item=self.plaid_item1)
        self.diff_plaid_account = mommy.make(PlaidAccount, item=self.different_plaid_item)

    def test_can_view_only_own_plaid_accounts(self):
        response = self.client.get('/api/plaid_accounts')
        content = json.loads(response.content)
        plaid_account_ids = [plaid_account.get('id') for plaid_account in content['plaid_accounts']]
        self.assertEqual(len(plaid_account_ids), 3)
        self.assertTrue(self.plaid_account1.id in plaid_account_ids)
        self.assertTrue(self.plaid_account2.id in plaid_account_ids)
        self.assertTrue(self.plaid_account3.id in plaid_account_ids)

    def test_can_only_update_own_plaid_account(self):
        data = {
            'is_roundup': True
        }
        response = self.client.patch('/api/plaid_accounts/%s' % self.plaid_account1.id, data)
        self.assertEqual(response.status_code, 200)

        # now try updating someone else's plaid_account
        response = self.client.patch('/api/organization_portfolio_compositions/%s' % self.diff_plaid_account.id, data)
        self.assertEqual(response.status_code, 404)

    def test_can_only_update_is_roundup_property(self):
        data = {
            'is_roundup': True
        }
        response = self.client.patch('/api/plaid_accounts/%s' % self.plaid_account1.id, data)
        self.assertEqual(response.status_code, 200)

        data['mask'] = '4325'
        response = self.client.patch('/api/plaid_accounts/%s' % self.plaid_account1.id, data)
        self.assertEqual(response.status_code, 400)

        data = {'name': 'some_name'}
        response = self.client.patch('/api/plaid_accounts/%s' % self.plaid_account1.id, data)
        self.assertEqual(response.status_code, 400)

    def test_plaid_account_is_readonly_or_update_only(self):
        data = {
            'is_roundup': False
        }

        # create should fail
        response = self.client.post('/api/plaid_accounts', data)
        self.assertEqual(response.status_code, 405)

        # delete should fail
        response = self.client.delete('/api/plaid_accounts/%s' % self.plaid_account1.id)
        self.assertEqual(response.status_code, 405)

        # put should fail
        response = self.client.put('/api/plaid_accounts/%s' % self.plaid_account1.id, data)
        self.assertEqual(response.status_code, 400)

        # patch should succeed
        response = self.client.patch('/api/plaid_accounts/%s' % self.plaid_account1.id, data)
        self.assertEqual(response.status_code, 200)

        # read should succeed
        response = self.client.get('/api/plaid_accounts')
        self.assertEqual(response.status_code, 200)
