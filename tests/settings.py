from slactivist.settings import *   # noqa

# overwrite any test settings here
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'testdatabase'
    }
}
