from django.db import IntegrityError
from django.test import TestCase
from model_mommy import mommy

from common.models import (
    Portfolio,
    PortfolioQuestion,
    PortfolioQuestionComposition,
    User,
)


class PortfolioQuestionCompositionTestCase(TestCase):
    def test_score_is_in_range(self):
        user = mommy.make(User)
        portfolio = mommy.make(Portfolio, profile=user.profile)
        question = mommy.make(PortfolioQuestion)
        comp = mommy.make(
            PortfolioQuestionComposition,
            portfolio=portfolio,
            portfolio_question=question,
            score=4
        )

        with self.assertRaises(IntegrityError):
            comp.score = -3
            comp.save()

        with self.assertRaises(IntegrityError):
            comp.score = 11
            comp.save()
