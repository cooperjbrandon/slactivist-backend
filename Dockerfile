# https://wsvincent.com/django-docker-postgresql/
# Pull base image
FROM python:3.6

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install dependencies
COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt

# Copy project
COPY . /code/

# Set work directory
WORKDIR /code
