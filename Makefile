# Optional Environment Parameters:
#	CONFIG: additional config to pass in, overrides config imported from Heroku.
#		Applies to `start`, `shell`, `run`.
#	CMD: Django command to run
#		Applies to `run`.
#	IMPORT: the name of a Heroku app to import environment from.
#		Applies to `start`, `shell`, `run`.
#	INSTALL_DIR: path to the local install/virtualenv folder.
#		Default: installed
#		Applies to all targets.
#	PORT: the port to run the development server on.
#		Default: 8000
#		Applies to `start`.
#	TEST: path to a test folder, file, or module.
#		Applies to `test`, `unit`, `integration`.
#		Should be given in PyTest notation, see https://pytest.org/latest/usage.html#specifying-tests-selecting-tests
#		Example usage: TEST=test_views.py make test
#					   TEST=test_views.py::ExampleTestCase make test
#					   TEST=test_views.py::ExampleTestCase::specifc_test make test
#					   TEST=permissions/test_plaid_item.py make test
#   PACKAGE: desired python package to install
# 	    Applies to 'install_package'
# 		EXAMPLE: PACKAGE=flake8 make install_package
# 	WORKERS: number of workers to scale to for heroku
#		Default: 1
#		Applies to `heroku_stop_worker`

INSTALL_DIR  ?= $(shell git rev-parse --show-toplevel)/installed
PORT         ?= 8000
SOURCE_DIRS  := slactivist common core tests
WORKERS      ?= 1

ifdef IMPORT
_IMPORTED_CONFIG := $(shell heroku config -s -a $(IMPORT))
$(info * Imported config from $(IMPORT): $(_IMPORTED_CONFIG))
endif

_FULL_CONFIG := $(_IMPORTED_CONFIG) $(CONFIG)

# pretty print
define header
	@tput setaf 6
	@echo "* $1"
	@tput sgr0
endef

environment:
	@test -d $(INSTALL_DIR) || virtualenv $(INSTALL_DIR)
	@. $(INSTALL_DIR)/bin/activate; pip install pip-tools

# Build/install the app
# Runs on every command
install: requirements.txt
	$(call header,"Installing")
	$(call header,"Updating dependencies")
	@. $(INSTALL_DIR)/bin/activate; pip install -q --upgrade pip==18.1
	@. $(INSTALL_DIR)/bin/activate; pip-sync
	@. $(INSTALL_DIR)/bin/activate; python setup.py -q install

install_package:
	@. $(INSTALL_DIR)/bin/activate; echo "$(PACKAGE)" >> requirements.in; pip-compile; pip-sync

# Start the development server
start:
	$(call header,"Starting development server")
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py runserver $(PORT)

# Start the development ssl server
start_ssl:
	$(call header,"Starting development server")
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py runsslserver $(PORT)

# Start the Django shell
shell:
	$(call header,"Starting shell")
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py shell

# Run any Django command
run:
	$(call header,"Running command: $(CMD)")
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py $(CMD)

# Migrate the test database
migrate:
	$(call header,"Running migrate")
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py migrate

# Create new migrations
migrations:
	$(call header,"Running migrations")
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py makemigrations

# Create new data migration
datamigration: install
	@. $(INSTALL_DIR)/bin/activate; $(_FULL_CONFIG) python manage.py makemigrations --empty common

# Run unit tests (all or by file)
test: unit
unit: unit_test
unit_test: lint unit_nolint
unit_nolint:
	$(call header,"Running unit tests")
	@. $(INSTALL_DIR)/bin/activate; pytest --ds=tests.settings -s tests/$(TEST)

test-no-sugar: unit-no-sugar
unit-no-sugar: unit-test-no-sugar
unit-test-no-sugar: lint unit-no-sugar-nolint
unit-no-sugar-nolint:
	$(call header,"Running unit tests")
	@. $(INSTALL_DIR)/bin/activate; python manage.py test --settings=tests.settings

# Removes build files in working directory
clean_working_directory:
	$(call header,"Cleaning working directory")
	@rm -rf ./build ./dist ./slactivst-backend.egg-info
	@find . -type f -name '*.pyc' -exec rm -rf {} \;

# Full clean
clean: clean_working_directory
	$(call header,"Cleaning all")
	@rm -rf $(INSTALL_DIR)

# Lint the whole project
lint: clean_working_directory
	$(call header,"Linting code")
	@. $(INSTALL_DIR)/bin/activate; find $(SOURCE_DIRS) -type f -name '*.py' -not -path '*/migrations/*' | xargs flake8

# start the celery worker on heroku
heroku_start_worker:
	$(call header,"heroku_start_worker")
	$(shell heroku ps:scale worker=$(WORKERS))

# stop the celery worker on heroku
heroku_stop_worker:
	$(call header,"heroku_stop_worker")
	$(shell heroku ps:scale worker=0)

heroku_worker_logs:
	$(call header,"heroku_worker_logs")
	$(shell heroku logs -t -p worker)
