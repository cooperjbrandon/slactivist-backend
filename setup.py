#!/usr/bin/env python
from setuptools import find_packages, setup

EXCLUDE_FROM_PACKAGES = []

setup(
    name='slactivist-backend',
    packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
    scripts=['manage.py'],
    version='0.1'
)
