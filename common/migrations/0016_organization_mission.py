# Generated by Django 2.1 on 2019-02-01 05:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0015_auto_20190130_0305'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='mission',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
