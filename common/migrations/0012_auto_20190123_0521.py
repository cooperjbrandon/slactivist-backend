# Generated by Django 2.1 on 2019-01-23 05:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0011_auto_20190119_0209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolioquestioncomposition',
            name='score',
            field=models.FloatField(),
        ),
    ]
