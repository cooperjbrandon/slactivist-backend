# Generated by Django 2.1 on 2019-02-12 04:00

import common.mixins
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0024_auto_20190205_0337'),
    ]

    operations = [
        migrations.CreateModel(
            name='StripeCustomer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated', models.DateTimeField(auto_now=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('source', models.CharField(max_length=256)),
                ('stripe_customer_id', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=(common.mixins.ModelDiffMixin, models.Model),
        ),
        migrations.RemoveField(
            model_name='plaidaccount',
            name='wire_type',
        ),
        migrations.AddField(
            model_name='plaidaccount',
            name='mask',
            field=models.CharField(blank=True, max_length=8, null=True),
        ),
        migrations.AlterField(
            model_name='plaidaccount',
            name='name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='stripecustomer',
            name='account',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, to='common.PlaidAccount'),
        ),
        migrations.AddField(
            model_name='stripecustomer',
            name='creator',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='common_stripecustomer_related', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='stripecustomer',
            name='profile',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, to='common.Profile'),
        ),
    ]
