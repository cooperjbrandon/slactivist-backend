from django.apps import apps
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

from common.tasks import add_organizations_to_portfolio

User = get_user_model()

"""
Create a profile when a user is created.
"""


@receiver(post_save, sender=User)
def create_profile_on_user_create(sender, instance, created, **kwargs):

    user = instance

    if created:
        Profile = apps.get_model('common', 'Profile')

        Profile.objects.create(user=user)


@receiver(post_save, sender='common.PortfolioQuestionComposition')
def add_orgs_to_portfolio_on_question_comp_complete(sender, instance, created, **kwargs):
    PortfolioQuestion = apps.get_model('common', 'PortfolioQuestion')
    total_questions = PortfolioQuestion.objects.count()

    # if we are on the last question, and the portfolio has not yet been marked as ready,
    # add charities to the portfolio.
    if instance.portfolio_question.order == total_questions:
        portfolio = instance.portfolio
        if portfolio.ready is False:
            print('calling `add_organizations_to_portfolio` task')
            add_organizations_to_portfolio.delay(portfolio.id)
