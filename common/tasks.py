from celery import shared_task
from django.db.models import Prefetch

from common.models import (
    Organization,
    OrganizationPortfolioComposition,
    OrganizationQuestionComposition,
    Portfolio,
    PortfolioQuestionComposition
)


@shared_task
def add_organizations_to_portfolio(portfolio_id):
    # charity algorithm:
    # 1) Get all organizations with the same focus
    # 2) for each of those filtered orgs, get the variance of each orgquestioncomp to
    #       portquestioncomp for a given portquestion
    # 3) sum all the variances by each each. sort by smallest variance first
    # 4) set the first X organizations on the portfolio

    # TODO: USE ANNOTATION/AGGREGATION FOR FASTER CALCULATION??

    print('TASK: add_organizations_to_portfolio')

    portfolio = Portfolio.objects.filter(
        id=portfolio_id
    ).select_related(
        'focus'
    ).prefetch_related(
        Prefetch(
            'portfolio_question_compositions',
            queryset=PortfolioQuestionComposition.objects.select_related('portfolio_question'),
            to_attr='compositions'
        )
    ).first()

    print('TASK: add_organizations_to_portfolio - filtering organizations')
    filtered_organizations = Organization.objects.filter(
        focus=portfolio.focus
    ).prefetch_related(
        Prefetch(
            'organization_question_compositions',
            queryset=OrganizationQuestionComposition.objects.select_related('portfolio_question'),
            to_attr='compositions'
        )
    )

    # setup question map
    question_map = {}
    for comp in portfolio.compositions:
        question_map[comp.portfolio_question.id] = comp

    # setup variance map
    variance_list = []

    # now calculate the variance
    for org in filtered_organizations:
        temp_total_variance = 0
        # find the variance for each comp. add to total
        for org_comp in org.compositions:
            question_id = org_comp.portfolio_question.id

            corresponding_portfolio_comp = question_map[question_id]
            variance = abs(org_comp.score - corresponding_portfolio_comp.score)
            temp_total_variance += variance

        # after looping through all the variances for an org, save the total in the variance_list
        variance_list.append({'organization': org, 'total_variance': temp_total_variance})

    # sort the list, smallest variance first.
    most_similar_orgs = sorted(variance_list, key=lambda k: k['total_variance'])
    orgs_to_add = []
    order = 0
    for index, data in enumerate(most_similar_orgs):
        active = index < 10  # First 10 orgs are 'active', rest are inactive
        org_port_comp = OrganizationPortfolioComposition(
            organization=data['organization'],
            portfolio=portfolio,
            order=order if active else 100,
            active=active
        )
        orgs_to_add.append(org_port_comp)
        order += 1

    print(
        'TASK: add_organizations_to_portfolio - adding {} orgs to portfolio'.format(len(orgs_to_add))
    )

    # add orgs to portfolio. set portfolio.ready = True
    OrganizationPortfolioComposition.objects.bulk_create(orgs_to_add)
    portfolio.ready = True
    portfolio.save()
