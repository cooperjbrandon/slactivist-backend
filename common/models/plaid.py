from django.db import models

from common.mixins import BaseModel


class PlaidItem(BaseModel):
    # This model stores access info for each Plaid-item linked, and allows us to
    # get all Plaid items for a particular user. NOTE: a PlaidItem represents
    # a banking instutition per user.
    profile = models.ForeignKey('common.Profile', related_name='plaid_items', on_delete=models.PROTECT)

    item_id = models.CharField(max_length=256)
    access_token = models.CharField(max_length=256)
    institution_id = models.CharField(max_length=256)
    institution_name = models.CharField(max_length=256)


class PlaidAccount(BaseModel):
    item = models.ForeignKey(PlaidItem, related_name='plaid_accounts', on_delete=models.CASCADE)

    is_roundup = models.BooleanField(default=False)
    plaid_account_id = models.CharField(max_length=256)
    mask = models.CharField(max_length=8, null=True, blank=True)
    name = models.CharField(max_length=256, null=True, blank=True)
    official_name = models.CharField(max_length=256, null=True, blank=True)
    subtype = models.CharField(max_length=256, null=True, blank=True)
    type = models.CharField(max_length=256, null=True, blank=True)


class StripeCustomer(BaseModel):
    profile = models.OneToOneField('common.Profile', on_delete=models.PROTECT)
    account = models.OneToOneField('common.PlaidAccount', on_delete=models.PROTECT)

    source = models.CharField(max_length=256)
    stripe_customer_id = models.CharField(max_length=256)
