from django.db import IntegrityError
from django.db import models

from common.mixins import BaseModel


class Portfolio(BaseModel):
    focus = models.ForeignKey('common.PortfolioFocus', related_name='portfolios', on_delete=models.PROTECT)
    profile = models.ForeignKey('common.Profile', related_name='portfolios', on_delete=models.CASCADE)
    questions = models.ManyToManyField(
        'common.PortfolioQuestion',
        through='PortfolioQuestionComposition',
        related_name='portfolios'
    )
    organizations = models.ManyToManyField(
        'common.Organization',
        through='OrganizationPortfolioComposition',
        related_name='portfolios'
    )

    active = models.BooleanField(default=True)
    name = models.CharField(max_length=64, null=True, blank=True)
    ready = models.BooleanField(default=False)  # to denote when a portfolio has added charities

    class Meta:
        unique_together = ('profile', 'name')


class PortfolioFocus(BaseModel):
    description = models.CharField(max_length=512)
    name = models.CharField(max_length=32, unique=True)

    class Meta:
        verbose_name_plural = 'portfolio focuses'

    def __str__(self):
        return 'PortfolioFocus - {}'.format(self.name)


class PortfolioQuestion(BaseModel):
    explanation = models.CharField(max_length=256)
    inquiry = models.CharField(max_length=128)
    name = models.CharField(max_length=32, unique=True)
    max_label = models.CharField(max_length=32)
    min_label = models.CharField(max_length=32)
    order = models.SmallIntegerField(default=10)
    purpose = models.CharField(max_length=256)

    def __str__(self):
        return 'PortfolioQuestion - {}; (Order - {})'.format(self.name, self.order)


class PortfolioQuestionComposition(BaseModel):
    portfolio = models.ForeignKey(
        'common.Portfolio',
        related_name='portfolio_question_compositions',
        on_delete=models.CASCADE
    )
    portfolio_question = models.ForeignKey(
        'common.PortfolioQuestion',
        related_name='portfolio_question_compositions',
        on_delete=models.CASCADE
    )

    score = models.FloatField()

    def save(self, *args, **kwargs):
        if self.score < 0 or self.score > 10:
            raise IntegrityError('"score" must be between 0 and 10')

        super(PortfolioQuestionComposition, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('portfolio', 'portfolio_question')
