from django.db import IntegrityError
from django.db import models

from common.mixins import BaseModel


class Organization(BaseModel):
    name = models.CharField(max_length=128)
    website_url = models.CharField(max_length=128, null=True, blank=True)
    mission = models.CharField(max_length=256, null=True, blank=True)
    focus = models.ForeignKey('common.PortfolioFocus', related_name='organizations', on_delete=models.PROTECT)
    questions = models.ManyToManyField(
        'common.PortfolioQuestion',
        through='OrganizationQuestionComposition',
        related_name='organizations'
    )

    active = models.BooleanField(default=True)


class OrganizationQuestionComposition(BaseModel):
    organization = models.ForeignKey(
        'common.Organization',
        related_name='organization_question_compositions',
        on_delete=models.CASCADE
    )
    portfolio_question = models.ForeignKey(
        'common.PortfolioQuestion',
        related_name='organization_question_compositions',
        on_delete=models.CASCADE
    )

    score = models.FloatField()

    def save(self, *args, **kwargs):
        if self.score < 0 or self.score > 10:
            raise IntegrityError('"score" must be between 0 and 10')

        super(OrganizationQuestionComposition, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('organization', 'portfolio_question')


class OrganizationPortfolioComposition(BaseModel):
    organization = models.ForeignKey(
        'common.Organization',
        related_name='organization_portfolio_compositions',
        on_delete=models.CASCADE
    )
    portfolio = models.ForeignKey(
        'common.Portfolio',
        related_name='organization_portfolio_compositions',
        on_delete=models.CASCADE
    )

    active = models.BooleanField(default=False)
    order = models.SmallIntegerField(default=100)
    is_manual_add = models.BooleanField(default=False)

    is_removed = models.BooleanField(default=False)
    removal_reason_bad_focus = models.BooleanField(default=False)
    removal_reason_bad_other = models.BooleanField(default=False)
    removal_reason_explanation = models.CharField(max_length=512, null=True, blank=True)
    removal_reason_bad_questions = models.ManyToManyField(
        'common.PortfolioQuestion',
        related_name='removed_reason_organization_portfolio_compositions'
    )

    class Meta:
        unique_together = ('organization', 'portfolio')
