=====
Slactivist Backend
=====
First time starting the server:
 - make sure you have virtualenv on your system:
```sh
$ pip install virtualenv
```
- now in the slactivist-backend directory, create the `installed` folder
```sh
$ virtualenv installed
```
- install the dependencies
```sh
$ make install
```
- start the server. get the config vars from the slactivist-backend. setup your shell so that you have access to the slactivist-backend heroku account.
```sh
$ IMPORT=master-slactivist-backend make start
```

All other times starting the server:
```sh
$ make install
$ IMPORT=master-slactivist-backend make start
```
