from random import uniform

from django.core.management.base import BaseCommand

from common.models import (
    Organization,
    OrganizationQuestionComposition,
    PortfolioFocus,
    PortfolioQuestion,
)


class Command(BaseCommand):
    help = 'creates organizations and organization-compositions (with scores)'

    def handle(self, *args, **options):
        for focus in PortfolioFocus.objects.all():
            orgs = []
            # create 30 organizations for each portfolio focus
            for i in range(30):
                org = Organization(
                    name='Organization {}'.format(i),
                    website_url='google.com',
                    focus=focus
                )
                orgs.append(org)

            Organization.objects.bulk_create(orgs)
            self.stdout.write(self.style.SUCCESS('Created 30 orgs for focus - {}'.format(focus.name)))

        # for each organization, create an org-question-comp for each portfolio-question
        for index, org in enumerate(Organization.objects.all()):
            if index % 30 == 0:
                self.stdout.write(self.style.SUCCESS('Creating org-question-comps or org # {}'.format(index)))

            for pq in PortfolioQuestion.objects.all():
                OrganizationQuestionComposition.objects.create(
                    organization=org,
                    portfolio_question=pq,
                    score=uniform(0, 10)
                )
