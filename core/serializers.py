from allauth.account.models import EmailAddress
from django.core.exceptions import ObjectDoesNotExist
from dynamic_rest.serializers import (
    DynamicModelSerializer,
)
from dynamic_rest.fields import (
    DynamicMethodField,
    DynamicRelationField
)

from common.models import (
    Organization,
    OrganizationPortfolioComposition,
    OrganizationQuestionComposition,
    PlaidAccount,
    PlaidItem,
    Portfolio,
    PortfolioFocus,
    PortfolioQuestion,
    PortfolioQuestionComposition,
    Profile,
    User,
)
from core.permissions import (
    FieldMarshall,
    SideGateKeeper,
)


class ApiSerializer(FieldMarshall, SideGateKeeper, DynamicModelSerializer):
    pass


class OrganizationSerializer(ApiSerializer):

    class Meta:
        model = Organization
        name = 'organization'
        fields = (
            'id',
            'mission',
            'name',
            'website_url',
            'focus',
            'organization_portfolio_compositions',
            'organization_question_compositions',
            'questions',
            'active',
            'portfolios',
        )
        deferred_fields = (
            'focus',
            'organization_portfolio_compositions',
            'organization_question_compositions',
            'portfolios',
            'questions',
        )

    focus = DynamicRelationField('PortfolioFocusSerializer')
    organization_portfolio_compositions = DynamicRelationField(
        'OrganizationPortfolioCompositionSerializer',
        many=True
    )
    organization_question_compositions = DynamicRelationField(
        'OrganizationQuestionCompositionSerializer',
        many=True
    )
    portfolios = DynamicRelationField('PortfolioSerializer', many=True)
    questions = DynamicRelationField('PortfolioQuestionSerializer', many=True)


class OrganizationPortfolioCompositionSerializer(ApiSerializer):

    class Meta:
        model = OrganizationPortfolioComposition
        name = 'organization_portfolio_composition'
        fields = (
            'id',
            'active',
            'is_manual_add',
            'is_removed',
            'order',
            'organization',
            'portfolio',
            'removal_reason_bad_focus',
            'removal_reason_bad_other',
            'removal_reason_explanation',
            'removal_reason_bad_questions',
        )
        deferred_fields = (
            'organization',
            'portfolio',
        )
        read_only_fields = (
            'order',
            'organization',
            'portfolio',
        )

    organization = DynamicRelationField('OrganizationSerializer')
    portfolio = DynamicRelationField('PortfolioSerializer')
    removal_reason_bad_questions = DynamicRelationField('PortfolioQuestionSerializer', many=True)


class OrganizationQuestionCompositionSerializer(ApiSerializer):

    class Meta:
        model = OrganizationQuestionComposition
        name = 'organization_question_composition'
        fields = (
            'id',
            'organization',
            'portfolio_question',
            'score',
        )
        deferred_fields = (
            'organization',
            'portfolio_question',
        )

    organization = DynamicRelationField('OrganizationSerializer')
    portfolio_question = DynamicRelationField('PortfolioQuestionSerializer')


class PlaidAccountSerializer(ApiSerializer):

    class Meta:
        model = PlaidAccount
        name = 'plaid_account'
        fields = (
            'id',
            'is_roundup',
            'item',
            'mask',
            'name',
            'official_name',
            'subtype',
            'type',
        )
        read_only_fields = (
            'item',
            'mask',
            'name',
            'official_name',
            'subtype',
            'type',
        )
        deferred_fields = (
            'item',
        )

    item = DynamicRelationField('PlaidItemSerializer')


class PlaidItemSerializer(ApiSerializer):

    class Meta:
        model = PlaidItem
        name = 'plaid_item'
        fields = (
            'id',
            'institution_id',
            'institution_name',
            'plaid_accounts',
            'profile',
        )
        deferred_fields = (
            'plaid_accounts',
            'profile',
        )

    plaid_accounts = DynamicRelationField('PlaidAccountSerializer', many=True)
    profile = DynamicRelationField('ProfileSerializer')


class PortfolioSerializer(ApiSerializer):

    class Meta:
        model = Portfolio
        name = 'portfolio'
        fields = (
            'id',
            'active',
            'focus',
            'name',
            'organizations',
            'organization_portfolio_compositions',
            'profile',
            'portfolio_question_compositions',
            'questions',
            'ready',
        )
        read_only_fields = (
            'active',
            'organizations',
            'organization_portfolio_compositions',
            'portfolio_question_compositions',
            'questions',
            'ready',
        )
        deferred_fields = (
            'focus',
            'organizations',
            'organization_portfolio_compositions',
            'profile',
            'portfolio_question_compositions',
            'questions',
        )

    focus = DynamicRelationField('PortfolioFocusSerializer')
    organizations = DynamicRelationField('OrganizationSerializer', many=True)
    organization_portfolio_compositions = DynamicRelationField(
        'OrganizationPortfolioCompositionSerializer',
        many=True
    )
    profile = DynamicRelationField('ProfileSerializer')
    portfolio_question_compositions = DynamicRelationField(
        'PortfolioQuestionCompositionSerializer',
        many=True
    )
    questions = DynamicRelationField('PortfolioQuestionSerializer', many=True)


class PortfolioFocusSerializer(ApiSerializer):

    class Meta:
        model = PortfolioFocus
        name = 'portfolio_focus'
        plural_name = 'portfolio_focuses'
        fields = (
            'id',
            'description',
            'name',
            'portfolios'
        )
        deferred_fields = (
            'portfolios',
        )

    portfolios = DynamicRelationField('PortfolioSerializer', many=True)


class PortfolioQuestionSerializer(ApiSerializer):

    class Meta:
        model = PortfolioQuestion
        name = 'portfolio_question'
        fields = (
            'id',
            'explanation',
            'inquiry',
            'name',
            'max_label',
            'min_label',
            'order',
            'organizations',
            'organization_question_compositions',
            'purpose',
            'portfolios',
            'portfolio_question_compositions',
        )
        deferred_fields = (
            'organizations',
            'organization_question_compositions',
            'portfolios',
            'portfolio_question_compositions',
        )

    organizations = DynamicRelationField('OrganizationSerializer', many=True)
    organization_question_compositions = DynamicRelationField(
        'OrganizationQuestionCompositionSerializer',
        many=True
    )
    portfolios = DynamicRelationField('PortfolioSerializer', many=True)
    portfolio_question_compositions = DynamicRelationField(
        'PortfolioQuestionCompositionSerializer',
        many=True
    )


class PortfolioQuestionCompositionSerializer(ApiSerializer):

    class Meta:
        model = PortfolioQuestionComposition
        name = 'portfolio_question_composition'
        fields = (
            'id',
            'portfolio',
            'portfolio_question',
            'score',
        )
        deferred_fields = (
            'portfolio',
            'portfolio_question',
        )

    portfolio = DynamicRelationField('PortfolioSerializer')
    portfolio_question = DynamicRelationField('PortfolioQuestionSerializer')


class ProfileSerializer(ApiSerializer):

    class Meta:
        model = Profile
        name = 'profile'
        fields = (
            'id',
            'has_funding_source',
            'user',
            'plaid_items',
            'portfolios',
        )
        deferred_fields = (
            'has_funding_source',
            'plaid_items',
            'portfolios',
            'user',
        )

    has_funding_source = DynamicMethodField(requires=['stripecustomer'])
    plaid_items = DynamicRelationField('PlaidItemSerializer', many=True)
    portfolios = DynamicRelationField('PortfolioSerializer', many=True)
    user = DynamicRelationField('UserSerializer')

    def get_has_funding_source(self, profile):
        try:
            profile.stripecustomer
            return True
        except ObjectDoesNotExist:
            return False


class UserSerializer(ApiSerializer):

    class Meta:
        model = User
        name = 'user'
        fields = (
            'email_verified',
            'date_joined',
            'email',
            'id',
            'is_active',
            'is_staff',
            'is_superuser',
            'first_name',
            'last_name',
            'profile',
        )
        deferred_fields = (
            'email_verified',
            'profile',
        )

    email_verified = DynamicMethodField()
    profile = DynamicRelationField('ProfileSerializer')

    def get_email_verified(self, user):
        return EmailAddress.objects.filter(user=user, verified=True).exists()
