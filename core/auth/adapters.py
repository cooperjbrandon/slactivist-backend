from allauth.account.adapter import DefaultAccountAdapter
from django.urls import reverse


class AuthAccountAdapter(DefaultAccountAdapter):

    def get_email_confirmation_url(self, request, emailconfirmation):
        # overwrite this method to return 'company_url.com/rest-auth/registration...'
        # instead of `production-slactivist-backend.com/rest-auth/registration...
        url = reverse(
            "account_confirm_email",
            args=[emailconfirmation.key])
        url = url.replace('verify-email/', 'verify-email?key=')
        ret = 'http://www.TBD_PLACEHOLDER.com' + url
        return ret
