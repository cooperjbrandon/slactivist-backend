{% load i18n %}{% autoescape off %}
{% blocktrans %}
Hello from <Company Name>!

You're receiving this e-mail because you have requested a password reset for your <Company Name> user account.
It can be safely ignored if you did not request a password reset. Click the link below to reset your password.
{% endblocktrans %}

{% trans "Please go to the following page and choose a new password:" %}
{% block reset_link %}
{{ protocol }}://{{ domain }}/reset?uid={{ uid }}&token={{ token }}
{% endblock %}
{% blocktrans %}

Thanks for using <Company Name>!
{% endblocktrans %}{% endautoescape %}
