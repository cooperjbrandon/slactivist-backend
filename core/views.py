from allauth.account.models import EmailAddress
from django.conf import settings
from django.db.models import Q
# from django.db.transaction import atomic
from dynamic_rest.viewsets import DynamicModelViewSet
from plaid import Client
# from rest_framework import exceptions
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import (
    IsAuthenticated,
)
from rest_framework.response import Response
import stripe

from core.mixins import (
    NoCreateMixin,
    NoDeleteMixin,
    NoUpdateMixin,
    ReadOnlyMixin,
    SaveMixin,
    AddCreatorMixin
)
# from core.utils import catch_failures
from common.models import (
    Organization,
    OrganizationPortfolioComposition,
    PlaidAccount,
    PlaidItem,
    Portfolio,
    PortfolioFocus,
    PortfolioQuestion,
    PortfolioQuestionComposition,
    Profile,
    StripeCustomer,
    User,
)
from core.permissions import (
    GateKeeper,
)
from core.serializers import (
    OrganizationSerializer,
    OrganizationPortfolioCompositionSerializer,
    PlaidAccountSerializer,
    PlaidItemSerializer,
    PortfolioSerializer,
    PortfolioFocusSerializer,
    PortfolioQuestionSerializer,
    PortfolioQuestionCompositionSerializer,
    ProfileSerializer,
    UserSerializer,
)

plaid_client = Client(
    client_id=settings.PLAID_CLIENT_ID,
    secret=settings.PLAID_SECRET,
    public_key=settings.PLAID_PUBLIC_KEY,
    environment=settings.PLAID_ENV
)
stripe.api_key = settings.STRIPE_API_KEY


class ApiViewSet(GateKeeper, AddCreatorMixin, SaveMixin, DynamicModelViewSet):
    pass


class OrganizationViewSet(ReadOnlyMixin, ApiViewSet):
    model = Organization
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (IsAuthenticated, )


class OrganizationPortfolioCompositionViewSet(NoCreateMixin, NoDeleteMixin, ApiViewSet):
    model = OrganizationPortfolioComposition
    queryset = OrganizationPortfolioComposition.objects.all()
    serializer_class = OrganizationPortfolioCompositionSerializer
    permission_classes = (IsAuthenticated, )


class PlaidAccountViewSet(NoCreateMixin, NoDeleteMixin, ApiViewSet):
    model = PlaidAccount
    queryset = PlaidAccount.objects.all()
    serializer_class = PlaidAccountSerializer
    permission_classes = (IsAuthenticated, )


class PlaidItemViewSet(NoUpdateMixin, NoDeleteMixin, ApiViewSet):
    model = PlaidItem
    queryset = PlaidItem.objects.all()
    serializer_class = PlaidItemSerializer
    permission_classes = (IsAuthenticated, )

    def create(self, request, *args, **kwargs):
        # get data from request
        public_token = request.data['plaid_item'].get('public_token')
        institution_name = request.data['plaid_item'].get('institution_name')
        institution_id = request.data['plaid_item'].get('institution_id')
        profile_id = request.data['plaid_item'].get('profile')

        # exchange public_token for private access_token and the item_id
        exchange_response = plaid_client.Item.public_token.exchange(public_token)
        plaid_access_token = exchange_response['access_token']
        plaid_item_id = exchange_response['item_id']

        # get the plaid accounts
        auth_response = plaid_client.Auth.get(plaid_access_token)

        # get the accounts which allow auth (direct withdrawals)
        auth_numbers = []
        for ach in auth_response['numbers']['ach']:
            auth_numbers.append(ach['account_id'])
        for eft in auth_response['numbers']['eft']:
            auth_numbers.append(eft['account_id'])

        # create a plaid_item in the db (institution)
        profile = Profile.objects.get(id=profile_id)
        plaid_item = PlaidItem.objects.create(
            profile=profile,
            access_token=plaid_access_token,
            item_id=plaid_item_id,
            institution_name=institution_name,
            institution_id=institution_id
        )

        # create plaid_accounts in the db for either auth or credit for the linked institution
        for account in auth_response['accounts']:
            if account['account_id'] in auth_numbers or account['type'] == 'credit':
                PlaidAccount.objects.create(
                    item=plaid_item,
                    plaid_account_id=account['account_id'],
                    mask=account['mask'],
                    name=account['name'],
                    official_name=account['official_name'],
                    subtype=account['subtype'],
                    type=account['type']
                )

        serializer = self.get_serializer(plaid_item)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def set_funding_source(self, request, *args, **kwargs):

        # get plaid account. Note - plaid account cannot be of type 'credit' and it must belong to the current user
        account_id = request.data['plaid_account']['id']
        plaid_account = PlaidAccount.objects.filter(
            Q(id=account_id, item__profile__user=request.user) & ~Q(type='credit')
        ).select_related('item__profile').first()

        plaid_access_token = plaid_account.item.access_token

        # plaid: exhange the plaid_access_token for the the stripe_bank_access token
        stripe_response = plaid_client.Processor.stripeBankAccountTokenCreate(
            plaid_access_token,
            plaid_account.plaid_account_id
        )
        stripe_bank_account_token = stripe_response['stripe_bank_account_token']

        # stripe: create a customer with the stripe_bank_account_token
        stripe_customer = stripe.Customer.create(
            description='Customer for profile {}'.format(plaid_account.item.profile.id),
            source=stripe_bank_account_token
        )

        # db: create a customer (stripe customer in which we can charge against)
        StripeCustomer.objects.create(
            profile=plaid_account.item.profile,
            account=plaid_account,
            source=stripe_bank_account_token,
            stripe_customer_id=stripe_customer['id']
        )

        return Response({}, status=201)


class PortfolioViewSet(NoDeleteMixin, ApiViewSet):
    model = Portfolio
    queryset = Portfolio.objects.all()
    serializer_class = PortfolioSerializer
    permission_classes = (IsAuthenticated, )


class PortfolioFocusViewSet(ReadOnlyMixin, ApiViewSet):
    model = PortfolioFocus
    queryset = PortfolioFocus.objects.all()
    serializer_class = PortfolioFocusSerializer
    permission_classes = (IsAuthenticated, )


class PortfolioQuestionViewSet(ReadOnlyMixin, ApiViewSet):
    model = PortfolioQuestion
    queryset = PortfolioQuestion.objects.all()
    serializer_class = PortfolioQuestionSerializer
    permission_classes = (IsAuthenticated, )


class PortfolioQuestionCompositionViewSet(NoDeleteMixin, ApiViewSet):
    model = PortfolioQuestionComposition
    queryset = PortfolioQuestionComposition.objects.all()
    serializer_class = PortfolioQuestionCompositionSerializer
    permission_classes = (IsAuthenticated, )


class ProfileViewSet(ReadOnlyMixin, ApiViewSet):
    model = Profile
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated, )


class UserViewSet(ReadOnlyMixin, ApiViewSet):
    """
    Users endpoint.

    Custom Behavior:
    * `GET /users/me`
        For the detail endpoint, passing `me` or `current` as the ID
        will return information about the current user.
    """
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, )

    def retrieve(self, request, *args, **kwargs):
        # users/me behavior
        pk = self.kwargs.get('pk', None)
        if pk and (pk == 'current' or pk == 'me'):
            self.kwargs['pk'] = request.user.id

        return super(UserViewSet, self).retrieve(request, *args, **kwargs)

    @action(detail=True, methods=['post'])
    def resend_verification_email(self, request, *args, **kwargs):
        if str(request.user.id) != kwargs['pk']:
            return Response({'detail': 'Not Found'}, status=404)

        email_address = EmailAddress.objects.get(email=request.user.email)
        email_address.send_confirmation(request=request)
        return Response({}, status=201)
