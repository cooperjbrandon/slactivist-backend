from rest_framework import exceptions
from common.models import (
    OrganizationPortfolioComposition,
    PlaidAccount,
    PlaidItem,
    Portfolio,
    PortfolioQuestionComposition,
)


class OrganizationPortfolioCompositionPermission(object):
    def check_field_permissions(self, data, request_user, instance=None):
        org_port_comp = instance  # readability

        active = data.get('active')

        # on create
        if instance is None:
            pass  # update only view

        # on update
        else:
            # cannot remove an org if it's the last org in the portfolio
            if (
                active is False and
                org_port_comp.portfolio.organization_portfolio_compositions.filter(active=True).count() == 1
            ):
                raise exceptions.ValidationError(
                    'Cannot remove an organization from the portfolio '
                    'if it\'s the last active organization in the portfolio'
                )


class PlaidAccountPermission(object):
    def check_field_permissions(self, data, request_user, instance=None):
        plaid_account = instance  # readability

        # on create - no creates allowed
        if instance is None:
            pass

        # on update
        else:
            # cannot only update 'is_roundup' property
            for key in data:
                if key != 'is_roundup' and data[key] != getattr(plaid_account, key):
                    raise exceptions.ValidationError('Can only update the is_roundup property')


class PlaidItemPermission(object):
    def check_field_permissions(self, data, request_user, instance=None):
        plaid_item = instance  # readability

        profile = data.get('profile')

        # on create
        if instance is None:
            # the user's profile must be the specified profile
            if profile.user != request_user:
                raise exceptions.ValidationError('You can only specify your own profile')

        # on update (note: updates not allowed, but just incase, add this check)
        else:
            # cannot update 'profile' property
            if profile and profile != plaid_item.profile:
                raise exceptions.ValidationError('Cannot change the profile of a portfolio')


class PortfolioPermission(object):
    def check_field_permissions(self, data, request_user, instance=None):
        portfolio = instance  # readability

        profile = data.get('profile')
        focus = data.get('focus')

        # on create
        if instance is None:
            # the user's profile must be the specified profile
            if profile.user != request_user:
                raise exceptions.ValidationError('You can only specify your own profile')

        # on update
        else:
            # cannot update 'profile' property
            if profile and profile != portfolio.profile:
                raise exceptions.ValidationError('Cannot change the profile of a portfolio')

            # cannot update the portfolio's focus if the portfolio is already 'ready'
            if focus and focus != portfolio.focus and portfolio.ready:
                raise exceptions.ValidationError(
                    'Cannot update a portfolio\'s focus after organizations '
                    'have already been added to the portfolio'
                )


class PortfolioQuestionCompositionPermission(object):
    def check_field_permissions(self, data, request_user, instance=None):
        portfolio_question_composition = instance  # readability

        portfolio = data.get('portfolio')
        portfolio_question = data.get('portfolio_question')

        # on create
        if instance is None:
            # the portfolio must belong to the user
            if portfolio.profile.user != request_user:
                raise exceptions.ValidationError('You can only specify your portfolios belonging to yourself')

        # on update
        else:
            # cannot update 'portfolio' property]
            if portfolio and portfolio != portfolio_question_composition.portfolio:
                raise exceptions.ValidationError(
                    'Cannot change the portfolio property of a portfolio_question_composition'
                )

            # cannot update 'portfolio_question' property
            if portfolio_question and portfolio_question != portfolio_question_composition.portfolio_question:
                raise exceptions.ValidationError(
                    'Cannot change the portfolio_question property of a portfolio_question_composition'
                )

            # cannot update a composition if the portfolio is already 'ready'
            if portfolio_question_composition.portfolio.ready:
                raise exceptions.ValidationError(
                    'Cannot update a portfolio_question_composition after organizations '
                    'have already been added to the portfolio'
                )


class FieldMarshall(object):
    """

    By default, if a user can create/update a model, they can change any field.
    Use this class to do permission checks on fields.
    """
    field_perm_mapping = {
        OrganizationPortfolioComposition: OrganizationPortfolioCompositionPermission(),
        PlaidAccount: PlaidAccountPermission(),
        PlaidItem: PlaidItemPermission(),
        Portfolio: PortfolioPermission(),
        PortfolioQuestionComposition: PortfolioQuestionCompositionPermission(),
    }

    def map_permission(self, validated_data, instance=None):
        permission = self.field_perm_mapping.get(self.Meta.model)
        user = self.context['request'].user

        # only check permission if there is a permission
        # if not user.is_superuser and permission:
        if permission:
            permission.check_field_permissions(validated_data, user, instance)

    def create(self, validated_data):
        self.map_permission(validated_data)
        return super(FieldMarshall, self).create(validated_data)

    def update(self, instance, validated_data):
        self.map_permission(validated_data, instance)
        return super(FieldMarshall, self).update(instance, validated_data)
